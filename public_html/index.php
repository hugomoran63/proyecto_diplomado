<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <title>Análisis técnico para inversión finaciera</title>


  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Nunito+Sans:wght@200&family=Raleway:wght@200&display=swap" rel="stylesheet">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
  <link rel="stylesheet" href="css/styles.css">
  <script src="https://kit.fontawesome.com/1c9ad4b785.js" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
  <script src="https://kit.fontawesome.com/37d826aeb4.js" crossorigin="anonymous"></script>
 <!--favicon-->
  <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
  <link rel="manifest" href="favicon/manifest.json">
  <meta name="msapplication-TileColor" content="#ffffff">
  <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
  <meta name="theme-color" content="#ffffff">

  <meta name="description" content="Todo sobre análisis técnico para hacer trading en todos los mercados financieros">

 <link
    rel="stylesheet"
    href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
  />

</head>

<body>
<div id="fb-root"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v15.0" nonce="yPVczhKU"></script>
 

 <div class="nav-barsec">
    <section id="title">

      <!-- Nav Bar -->
      <nav class="navbar navbar-expand-lg navbar-dark">
        <a class="navbar-brand" href="#">
          <img src="images/crypto.png" width="55" height="55" class="d-inline-block align-top" alt="">
          CRYPTO
        </a>
        
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
        <ul class="navbar-nav ms-auto ">
            <li class="nav-item">
                <a class="nav-link" href="index.php">INICIO</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="velas.html">VELAS JAPONESAS</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="patrones.html">PATRONES CHARTISTAS</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="indicadores.html">INDICADORES TÉCNICOS</a>
          </li>
            <li class="nav-item">
              <a class="nav-link" href>MISCELANEA</a>
          </li>
            <li class="nav-item">
              <a class="nav-link" href>HERRAMIENTA</a>
          </li>
        </ul>
        </div>
    </nav>
  </div>

  <section id="carrusel">

    <div id="demo" class="carousel slide" data-bs-ride="carousel">

      <!-- Indicators/dots -->
      <div class="carousel-indicators">
          <button type="button" data-bs-target="#demo" data-bs-slide-to="0" class="active"></button>
          <button type="button" data-bs-target="#demo" data-bs-slide-to="1"></button>
          <button type="button" data-bs-target="#demo" data-bs-slide-to="2"></button>
          <button type="button" data-bs-target="#demo" data-bs-slide-to="3"></button>
      </div>
      <!-- The slideshow/carousel -->
      <div class="carousel-inner">
          <div class="carousel-item active">
              <img src="images/carrusel1.png" alt="foto1" class="d-block w-100">
          </div>
          <div class="carousel-item">
              <img src="images/carrusel2.png" alt="foto2A" class="d-block w-100">
          </div>
          <div class="carousel-item">
              <img src="images/carrusel3.png" alt="foto3A" class="d-block w-100">
          </div>
          <div class="carousel-item">
              <img src="images/carrusel4.png" alt="Ejemplo de análisis técnico de activo de un portafolio" class="d-block w-100">
          </div>
      </div>

      <!-- Left and right controls/icons -->
      <button class="carousel-control-prev" type="button" data-bs-target="#demo" data-bs-slide="prev">
      <span class="carousel-control-prev-icon"></span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#demo" data-bs-slide="next">
      <span class="carousel-control-next-icon"></span>
      </button>
  </div>


  </section>
  <!-- Features -->
  
  
  
  <section id="Análisis técnico">

    <h1 class="wow animate__animated animate__backInRight">Análisis técnico</h1>

    <div class='row'>
      <div class='objetivos col-lg-4 wow animate__animated animate__backInRight'>
        <i class="icon fas fa-check-circle fa-4x"></i>
        <h3>Fácil acceso.</h3>
        <p>Crear un compendio de información relacionada al mundo de las criptomonedas.</p>
      </div>

      <div class='objetivos col-lg-4 wow animate__animated animate__backInRight'>
        <i class="icon fas fa-gear fa-4x"></i>
        <h3>Herramienta Elite</h3>
        <p>Todo lo que necesitas para visualizar los indicadores.</p>
      </div>

      <div class='objetivos col-lg-4 wow animate__animated animate__backInRight'>
        <i class="icon fas fa-lightbulb fa-4x "></i>
        <h3>Estrategias de trading</h3>
        <p>Te ayudaremos para fortalecer tus conocimientos sobre análisis técnico.</p>
      </div>
    </div>
  </section>

  <!--Crpytos-->
  <section id="info-cryptos">
    <h2>Cryptos tendencia</h2>
    <p class="p-text">Conoce las criptomonedas más populares para invertir</p>

    <div class="card-group crypto-cards wow animate__animated animate__backInDown ">

      <div class="card">
        <img class="card-img-top img-fluid" src="images/bitcoin.png" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Bitcoin</h5>
          <p class="card-text">Es una criptomoneda (un tipo de moneda digitalnota y un sistema de pago sin banco central o administrador único. En principio, los usuarios de bitcoin pueden transferir dinero entre sí a través de una red entre iguales usando software libre y de código abierto.</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">Conoce mas aquí</small>
        </div>
      </div>
  
  
      <div class="card">
        <img class="card-img-top img-fluid" src="images/cardano.png" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Cardano</h5>
          <p class="card-text">Es una cadena de bloques de código abierto, así como una plataforma para ejecutar contratos inteligentes y emitir su propia moneda digital, el ada. Cardano fue fundada en 2015 por el cofundador de Ethereum, Charles Hoskinson..</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">Conoce mas aquí</small>
        </div>
      </div>
      <div class="card">
        <img class="card-img-top img-fluid" src="images/litecoin.png" alt="Card image cap">
        <div class="card-body">
          <h5 class="card-title">Litecoin</h5>
          <p class="card-text">Litecoin (signo: Ł; abr.: LTC) es una criptomoneda y proyecto de software de código abierto publicado bajo la licencia MIT inspirado y prácticamente idéntico en su aspecto técnico a Bitcoin (BTC). Su creación y transferencia se basa en un protocolo criptográfico no administrado por ninguna autoridad central y sustentado en el consenso de una red peer-to-peer.</p>
        </div>
        <div class="card-footer">
          <small class="text-muted">Conoce mas aquí</small>
        </div>
      </div>
    </div>

  </section>
    
     <section style="background-color: #f1f1f1" class="wow animate__animated animate__backInLeft" >
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="fb-page" data-href="https://www.facebook.com/CoinMarketCap/" data-tabs="timeline" data-width="350"
 data-height="350" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
<blockquote cite="https://www.facebook.com/facebook" class="fb-xfbml-parse-ignore">
<a href="https://www.facebook.com/facebook">Facebook</a></blockquote></div></div>
        <div class="col-md-8">
          <form action="action_page.php" method="post">
  <div class="container">
    <h2>Subscribe to our Newsletter</h2>
    <p>No te pierdas el boletín semanal del sitio, tendrás los mejores artículos y análisis de la semana.</p>
  </div>

  <div class="container" style="background-color:white">
    <input type="text" placeholder="Name" name="nombre" required>
    <input type="text" placeholder="Email address" name="correo" required>
  </div>

  <div class="container">
    <input type="submit" value="Subscribe">
  </div>
</form>
</div>

      </div>

<h2>Noticiero Semanal </h2>
 <audio controls class="w-100">
  <source src="multimedia/noticia_semanal.m4a" type="audio/ogg">
Your browser does not support the audio element.
</audio>
    </div>

  </section>



  <!-- Footer -->


<script src="js/wow.min.js"></script>
              <script>
              new WOW().init();
              </script>
  <footer id="footer">

    <p class="p-footer">Descargo de responsabilidad: la información y las publicaciones no pretenden ser, y no constituyen, asesoramiento o recomendaciones financieras, de inversión, comerciales o de otro tipo proporcionadas o respaldadas por TradingView. Lea más en los Términos de uso.</p>

  </footer>

</body>

</html>

